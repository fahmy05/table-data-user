import React, { Component } from 'react'
import {Table,TableBody,TableCell,TableContainer,TableHead,TableRow,Paper,Typography,Breadcrumbs,TextField,Button,Autocomplete,Stack,CircularProgress,Snackbar,Alert} from '@mui/material';
import { Container } from '@mui/system';
import axios from 'axios';

export default class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      row:[],
      rowFilterd:[],
      genderFilter:['All','Female','Male'],
      open:false,
      rowResult:20,
      severity:'success',
      message:'',
      keysFilter:'',
      genderSelected:'Female',
      isLoading:false
    }
  }

  _setOpenToast(){
    this.setState({open:true})
  }

  _setCloseToast(){
    this.setState({open:false})
  }

  _resetFilter = () => {
    this.setState({genderSelected:'All'})
    this.setState({keysFilter:''})
    this._readData('All',this.state.rowResult)
  }

  _readData = (
    gender=this.state.genderSelected,
    countResults=this.state.rowResult
  ) => {
    this.setState({isLoading:true})
    axios
      .get('https://randomuser.me/api/?results='+countResults+(gender=='All'?'':'&gender='+gender.toLowerCase()), {
        headers: { 
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        },
      })
      .then((response) => {
        this.setState({row:response.data.results})
        this.setState({rowFilterd:response.data.results})
        this.setState({isLoading:false})
        this.setState({message:'Get Data Success'})
        this.setState({severity:'success'})
        this._setOpenToast();
        this._loadData(this.state.keysFilter,response.data.results)
      })
      .catch((err)=>{
        this.setState({message:'Get Data Failed'})
        this.setState({severity:'error'})
        this._setOpenToast();
      });

  }

  _loadData = (
    filterKey=this.state.keysFilter,
    row=this.state.row
  ) => {
    let arrayFiltered = [];
    if(filterKey!==''){
      arrayFiltered = row.filter(function (el) {
        return  el.login.username.includes(filterKey) ||
                (el.name.first+" "+el.name.last).includes(filterKey) ||
                el.email.includes(filterKey);
      });
      this.setState({rowFilterd:arrayFiltered})
    }else{
      this.setState({rowFilterd:row})
    }
  }

  _handleScroll = () => {
    if ((window.scrollY + window.innerHeight + 2) >= document.body.offsetHeight) {
        this.setState({rowResult:this.state.rowResult+=20})
        this._readData(this.state.genderSelected,this.state.rowResult)
    }
  }

  componentDidMount() {
    this._readData()
    document.addEventListener('scroll', this._handleScroll);
  }
  
  componentWillUnmount() {
    document.removeEventListener('scroll', this._handleScroll);
  }

  render() {
    return (
      <Container>
        <Stack spacing={1} direction='column'>
          <Breadcrumbs aria-label="breadcrumb">
            <Typography color="text.primary">Home</Typography>
            <Typography color="text.primary" fontWeight={700}>Example Page</Typography>
          </Breadcrumbs>
          <Typography
            color={"black"}
            fontWeight={700}
            fontSize="32px"
          >
            Example With Search and Filter
          </Typography>
          <Stack
            spacing={1}
            direction='row'
          >
            <TextField label="Search" variant="outlined" value={this.state.keysFilter} onChange={(e)=>{
              this.setState({keysFilter:e.target.value})
              this._loadData(e.target.value)
            }} />
            <Autocomplete
              value={this.state.genderSelected}
              onChange={(event, newValue) => {
                this.setState({genderSelected:newValue});
                this._readData(newValue,this.state.rowResult)
              }}
              options={this.state.genderFilter}
              sx={{ width: 300 }}
              renderInput={(params) => <TextField {...params} label="All" />}
            />
            <Button variant="contained" onClick={()=>this._resetFilter()}>Reset Filter</Button>
          </Stack>
          
          {this.state.rowFilterd.length > 0 && <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align='center'>No.</TableCell>
                  <TableCell>Username</TableCell>
                  <TableCell>Name</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>Gender</TableCell>
                  <TableCell>Registered Date</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.rowFilterd.map((row,i) => (
                  <TableRow
                    key={i}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row" align='center'>
                      {i+1}
                    </TableCell>
                    <TableCell>
                      {row.login.username}
                    </TableCell>
                    <TableCell>{row.name.first+" "+row.name.last}</TableCell>
                    <TableCell>{row.email}</TableCell>
                    <TableCell>{row.gender}</TableCell>
                    <TableCell>{row.registered.date}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>}
          {this.state.rowFilterd.length < 1 && <Typography
            color={"black"}
            fontWeight={700}
            fontSize="32px"
          >
            Data Not Found
          </Typography>}
          <CircularProgress style={{alignSelf:'center',display:this.state.isLoading?'flex':'none'}}/>
          <Snackbar
            open={this.state.open}
            autoHideDuration={6000}
            onClose={()=>{
              this._setCloseToast()
            }}>
              <Alert
                severity={this.state.severity}
                onClose={()=>{
                  this._setCloseToast()
                }}
              >{this.state.message}</Alert>
          </Snackbar>
        </Stack>
      </Container>
    );
  }
}
